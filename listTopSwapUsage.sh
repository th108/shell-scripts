#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# Source: https://superuser.com/questions/300004/how-can-i-find-out-processes-are-using-swap-space

for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | sort -k 2 -n -r | head -15
