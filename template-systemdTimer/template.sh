#!/usr/bin/env sh

BACKUP_NUM_KEEP=10

# healthchecks.io
#HC_URL=

# call external script
#./template2.sh

# keep only last backups
(ls -t /location/data/*.tar.gz | head -n $BACKUP_NUM_KEEP; ls /location/data/*.tar.gz) | sort | uniq -u | xargs -r rm -v

if ping -q -c 2 -W 1 1.1.1.1 >/dev/null;
  then
    rclone sync /location/template/ scaleway: --contimeout=15s --retries 2 --timeout=30s;
  else
    echo "Failure: IPv4 is down!"
fi
