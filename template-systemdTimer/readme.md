By default, systemd will only run timers if the user is logged in so to be able to run timer jobs without logged in we enable lingering session with

`sudo loginctl enable-linger username`

symlink (src dest)

`ln -s /home/user/projects/backup/vps-backup.timer /home/user/.config/systemd/user/vps-backup.timer`
