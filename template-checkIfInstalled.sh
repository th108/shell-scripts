#!/usr/bin/env bash

if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v cargo)" ]; then
  echo 'Error: cargo is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v gem)" ]; then
  echo 'Error: gem is not installed.' >&2
  exit 1
fi
