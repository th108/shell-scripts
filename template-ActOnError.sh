#!/usr/bin/env bash

#set -o errexit
set -o nounset
set -o pipefail

###
# depending on success (or failure) execute commands for the case
###

if [ $? -ne 0 ]
then
    echo "fail"
else
    echo "success"
fi
