#!/bin/bash -e
# Creates a systemd-nspawn container with Arch Linux
# Source: https://gist.github.com/sfan5/52aa53f5dca06ac3af30455b203d3404

MIRROR=https://ftp.sh.cvut.cz/arch
ISO_DATE=2019.01.01
PACKAGES="base"

if [ $UID -ne 0 ]; then
	echo "run this script as root" >&2
	exit 1
fi

if [ -z "$1" ]; then
	echo "Usage: $0 <destination>" >&2
	exit 0
fi

dest="$1"
tarfile=$(mktemp)

wget $MIRROR/iso/$ISO_DATE/archlinux-bootstrap-$ISO_DATE-x86_64.tar.gz -O $tarfile
mkdir "$dest"
tar -xzf $tarfile -C "$dest" --strip-components=1

printf 'Server = %s/$repo/os/$arch\n' $MIRROR >"$dest"/etc/pacman.d/mirrorlist
systemd-nspawn -q -D "$dest" sh <<SCRIPT
pacman-key --init
pacman-key --populate archlinux

pacman -Sy
# avoid installing the kernel
pkgs=\$(pacman -Sg $PACKAGES | while read _ p;do [[ "\$p" == "linux"* ]]||echo -n "\$p ";done)
pacman -S --noconfirm --needed \$pkgs
SCRIPT

rm $tarfile
echo ""
echo "Arch $ISO_DATE container was created successfully."
