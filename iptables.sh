#!/usr/bin/env sh

ipt='/sbin/iptables'
ip6t='/sbin/ip6tables'

####################### IPv4 ###########################
# Flush previous rules, delete chains and reset counters
$ipt -F
$ipt -X
$ipt -Z
$ipt -t nat -F

$ipt -P FORWARD DROP
$ipt -P INPUT   DROP
$ipt -P OUTPUT  ACCEPT

# Allow Loopback Connections
$ipt -A INPUT -i lo -j ACCEPT -m comment --comment "allow loopback connections"

# Allow Traceroute traffic
$ipt -I INPUT -p udp --dport 33434:33474 -j REJECT

# Allow Ping from outside
$ipt -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

$ipt -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
$ipt -A INPUT -p tcp --dport 22 -j ACCEPT
$ipt -A INPUT -p tcp --dport 80 -j ACCEPT
$ipt -A INPUT -p tcp --dport 443 -j ACCEPT

####################### IPv6 ###########################
# Flush previous rules, delete chains and reset counters
$ip6t -F
$ip6t -X
$ip6t -Z
$ip6t -t nat -F

$ip6t -P FORWARD DROP
$ip6t -P INPUT   DROP
$ip6t -P OUTPUT  ACCEPT

# Allow Loopback Connections
$ip6t -A INPUT -i lo -j ACCEPT -m comment --comment "allow loopback connections"

# Allow Traceroute traffic
$ipt6 -I INPUT -p udp --dport 33434:33474 -j REJECT

# Allow Ping from outside
$ipt6 -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

$ip6t -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
$ip6t -A INPUT -p tcp --dport 22 -j ACCEPT
$ip6t -A INPUT -p tcp --dport 80 -j ACCEPT
$ip6t -A INPUT -p tcp --dport 443 -j ACCEPT